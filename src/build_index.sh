#!/bin/bash

shopt -s nullglob
notebooks=(notebooks/*.html)

echo "<!doctype html>"
echo "<html>"
echo "<head>"
echo "<title>Research Notebooks</title>"
echo "</head>"
echo "<body>"
echo '<p><a href="2016/index.html">2016 notebooks</a>'
for var in "${notebooks[@]}"; do
    var2=${var##*/}
    echo "<p><a href=\"$var2\">${var2%.*}</a></p>"
done
echo "</body>"
 
echo "</html>"
