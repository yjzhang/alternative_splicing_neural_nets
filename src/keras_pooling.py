# Some implementations of advanced pooling layers for convolutional neural networks

import numpy as np
import theano.tensor as T
from keras.layers.core import Layer


# based on https://github.com/fchollet/keras/issues/373
# I have no idea how it works though
class KMaxPooling(Layer):

    def __init__(self, pooling_size):
        self.supports_masking=True
        super(KMaxPooling, self).__init__()
        self.pooling_size = pooling_size

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], self.pooling_size,
                input_shape[2])

    def get_output_mask(self, train=False):
        return None

    def call(self, x, mask=None):
        data = x
        if mask is None:
            mask = T.sum(T.ones_like(data), axis=-1)
        mask = mask.dimshuffle(0, 1, "x")
        masked_data = T.switch(T.eq(mask, 0), -np.inf, data)
        result = masked_data[T.arange(masked_data.shape[0]).dimshuffle(0, 'x', 'x'),
                T.sort(T.argsort(masked_data, axis=1)[:, -self.pooling_size:, :]),
                T.arange(masked_data.shape[2]).dimshuffle('x', 'x', 0)]
        return result

    def get_config(self):
        return {'name': self.__class__.__name__, 'pooling_size': self.pooling_size}

# This is a layer that simply sums up all the rows of the previous layer, 
# no weights. Advantage: can take an arbitrary number of inputs.
class SumLayer(Layer):

    def __init__(self):
        self.supports_masking=True
        super(SumLayer, self).__init__()

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], 1,
                input_shape[2])

    def get_output_mask(self, train=False):
        return None

    def call(self, x, mask=None):
        data = x
        if mask is None:
            mask = T.sum(T.ones_like(data), axis=-1)
        mask = mask.dimshuffle(0, 1, "x")
        masked_data = T.switch(T.eq(mask, 0), -np.inf, data)
        result = T.sum(masked_data, axis=1)
        return result

    def get_config(self):
        return {'name': self.__class__.__name__}

# okay so... this is kind of ridiculous but like, this combines sum and
# max pooling in one layer. It runs over the entire previous sequence and
# returns the sum and max for each channel of the previous layer.
class SumMaxPool(Layer):

    def __init__(self):
        self.supports_masking=True
        super(SumMaxPool, self).__init__()

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], 2,
                input_shape[2])

    def get_output_mask(self, train=False):
        return None

    def call(self, x, mask=None):
        data = x
        if mask is None:
            mask = T.sum(T.ones_like(data), axis=-1)
        mask = mask.dimshuffle(0, 1, "x")
        masked_data = T.switch(T.eq(mask, 0), -np.inf, data)
        sums = T.sum(masked_data, axis=1)
        maxes = T.max(masked_data, axis=1)
        result = T.join(1, sums, maxes)
        return result

    def get_config(self):
        return {'name': self.__class__.__name__}


