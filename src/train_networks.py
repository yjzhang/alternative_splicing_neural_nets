"""
Training neural networks...
"""
import os

from keras.models import Model, Sequential, model_from_json
from keras.layers import Input, concatenate, Convolution1D, LSTM, Dense, Flatten, Activation, Dropout, GlobalMaxPooling1D, GlobalAveragePooling1D
from keras.callbacks import EarlyStopping
import numpy as np
from sklearn.metrics import r2_score

import nn_utils
import draw_logo
import genomic_data

def load_data_for_training(cell_type='HEK'):
    X_train, X_test, Y_train, Y_test = nn_utils.load_cell_type_data(flank=0, cell_type=cell_type)
    # TODO: try to predict ratio of SD2 to SD2 + SD1: Y_test[:,44]/(Y_test[:,44] + Y_test[:,0])
    # TODO: train a splice site model - take a certain window around each splice site...
    Y2_test = Y_test[:,44]/(Y_test[:,44] + Y_test[:,0] + 1e-8)
    Y2_train = Y_train[:,44]/(Y_train[:,44] + Y_train[:,0] + 1e-8)
    X1_train = X_train.map(lambda x: x[0:25])
    X1_test = X_test.map(lambda x: x[0:25])
    #X1_train = X1_train.map(lambda x: np.rollaxis(x, 1))
    #X1_test = X1_test.map(lambda x: np.rollaxis(x, 1))
    X1_train = np.dstack(X1_train)
    X1_train = np.rollaxis(X1_train, 2)
    X1_test = np.dstack(X1_test)
    X1_test = np.rollaxis(X1_test, 2)
    Y2_train = Y2_train.reshape((len(Y2_train), 1))
    Y2_test = Y2_test.reshape((len(Y2_test), 1))
    return X1_train, X1_test, Y2_train, Y2_test

def load_data_for_training_2(cell_type='HEK'):
    """
    Loads both degenerate regions
    """
    X_train, X_test, Y_train, Y_test = nn_utils.load_cell_type_data(flank=0, cell_type=cell_type)
    Y2_test = Y_test[:,44]
    Y2_train = Y_train[:,44]
    X1_train = X_train.map(lambda x: x[0:25])
    X1_test = X_test.map(lambda x: x[0:25])
    X2_train = X_train.map(lambda x: x[25:50])
    X2_test = X_test.map(lambda x: x[25:50])
    #X1_train = X1_train.map(lambda x: np.rollaxis(x, 1))
    #X1_test = X1_test.map(lambda x: np.rollaxis(x, 1))
    X1_train = np.dstack(X1_train)
    X1_train = np.rollaxis(X1_train, 2)
    X1_test = np.dstack(X1_test)
    X1_test = np.rollaxis(X1_test, 2)
    Y2_train = Y2_train.reshape((len(Y2_train), 1))
    Y2_test = Y2_test.reshape((len(Y2_test), 1))
    return [X1_train, X2_train], [X1_test, X2_test], Y2_train, Y2_test


def train_network(X_train, Y_train, X_val, Y_val, model):
    early_stopping = EarlyStopping(monitor='val_loss', patience=1)
    model.fit(X_train, Y_train,
            epochs=50,
            batch_size=500,
            validation_data=(X_val, Y_val),
            #verbose=2,
            callbacks=[early_stopping])
    return model

def build_global_max_model(num_filters, filter_length):
    # model type 1: global max pooling w/1 conv layer,
    model1 = Sequential()
    model1.add(Convolution1D(num_filters[0], filter_length[0], activation='relu', input_shape=(None, 4)))
    #model1.add(Dropout(0.25))
    model1.add(GlobalMaxPooling1D())
    model1.add(Dense(units=1, activation='linear'))
    model1.add(Activation('sigmoid'))
    model1.compile(loss='binary_crossentropy',
                          optimizer='adam')
    return model1

def build_global_2_max_model(num_filters, filter_length):
    # model type 1: global max pooling w/2 conv layer,
    model1 = Sequential()
    model1.add(Convolution1D(num_filters[0], filter_length[0], activation='relu', input_shape=(None, 4)))
    #model1.add(Dropout(0.25))
    model1.add(Convolution1D(num_filters[1], filter_length[1], activation='relu'))
    model1.add(GlobalMaxPooling1D())
    model1.add(Dense(units=1, activation='linear'))
    model1.add(Activation('sigmoid'))
    model1.compile(loss='binary_crossentropy',
                          optimizer='adam')
    return model1

def build_global_mean_model(num_filters, filter_length):
    # model type 2: global avg pooling w/1 conv layer
    model1 = Sequential()
    model1.add(Convolution1D(num_filters[0], filter_length[0], activation='relu', input_shape=(None, 4)))
    #model1.add(Dropout(0.25))
    model1.add(GlobalAveragePooling1D())
    model1.add(Dense(units=1, activation='linear'))
    model1.add(Activation('sigmoid'))
    model1.compile(loss='binary_crossentropy',
                          optimizer='adam')
    return model1

def build_global_2_mean_model(num_filters, filter_length):
    # model type 2: global avg pooling w/2 conv layer
    model1 = Sequential()
    model1.add(Convolution1D(num_filters[0], filter_length[0], activation='relu', input_shape=(None, 4)))
    #model1.add(Dropout(0.25))
    model1.add(Convolution1D(num_filters[1], filter_length[1], activation='relu'))
    model1.add(GlobalAveragePooling1D())
    model1.add(Dense(units=1, activation='linear'))
    model1.add(Activation('sigmoid'))
    model1.compile(loss='binary_crossentropy',
                          optimizer='adam')
    return model1

def build_global_max_avg_model(num_filters, filter_length):
    # model type 4: global avg pooling + global max pooling w/1 conv layer
    # double the number of conv units
    input1 = Input(shape=(None,4))
    x1 = Convolution1D(num_filters[0], filter_length[0], activation='relu')(input1)
    x2 = Convolution1D(num_filters[0], filter_length[0], activation='relu')(input1)
    x1 = GlobalAveragePooling1D()(Dropout(0.25)(x1))
    x2 = GlobalMaxPooling1D()(Dropout(0.25)(x2))
    x = concatenate([x1, x2])
    x = Dense(units=1, activation='linear')(x)
    x = Activation('sigmoid')(x)
    model1 = Model(inputs=input1, outputs=x)
    model1.compile(loss='binary_crossentropy',
                          optimizer='adam')
    return model1

def build_global_2_max_avg_model(num_filters, filter_length):
    # model type 4: global avg pooling + global max pooling w/2 conv layers
    # double the number of conv units
    input1 = Input(shape=(None,4))
    x1 = Convolution1D(num_filters[0], filter_length[0], activation='relu')(input1)
    x2 = Convolution1D(num_filters[0], filter_length[0], activation='relu')(input1)
    x1 = Convolution1D(num_filters[1], filter_length[1], activation='relu')(x1)
    x2 = Convolution1D(num_filters[1], filter_length[1], activation='relu')(x2)
    x1 = GlobalAveragePooling1D()(Dropout(0.25)(x1))
    x2 = GlobalMaxPooling1D()(Dropout(0.25)(x2))
    x = concatenate([x1, x2])
    x = Dense(units=1, activation='linear')(x)
    x = Activation('sigmoid')(x)
    model1 = Model(inputs=input1, outputs=x)
    model1.compile(loss='binary_crossentropy',
                          optimizer='adam')
    return model1

def build_global_positional_model(num_filters, filter_length):
    # model type 3: no pooling, conv + dense
    model1 = Sequential()
    model1.add(Convolution1D(num_filters[0], filter_length[0], activation='relu', input_shape=(25, 4)))
    model1.add(Flatten())
    model1.add(Dense(units=1, activation='linear'))
    model1.add(Activation('sigmoid'))
    model1.compile(loss='binary_crossentropy',
                          optimizer='adam')
    return model1

def build_recurrent_model(num_filters, filter_length):
    # model type 4: conv + recurrent
    model1 = Sequential()
    model1.add(Convolution1D(num_filters[0], filter_length[0], activation='relu', input_shape=(None, 4)))
    model1.add(LSTM(units=num_filters[1]))
    model1.add(Dense(units=1, activation='linear'))
    model1.add(Activation('sigmoid'))
    model1.compile(loss='binary_crossentropy',
                          optimizer='adam')
    return model1

def train_all(model_builder, path, num_filters=None, filter_length=None,
        conv_layer_ids = None, use_both_regions = False):
    """
    Args:
        model_builder (function): takes two lists, num_filters and
            filter_length, and returns a compiled sequential model.
        path (str): base directory to store the models/outputs
    """
    cell_types = ['HEPG2', 'MCF7', 'CHO', 'HELA', 'LNCAP', 'HEK']
    wt_seqs, mut_seqs, wt_psi, hal_pred, actual_dpsi = genomic_data.load_hal_test_data()
    if num_filters is None:
        num_filters = [200,64,32]
    if filter_length is None:
        filter_length = [6,6,6]
    if conv_layer_ids is None:
        conv_layer_ids = [0]
    try:
        os.makedirs(path)
    except:
        pass
    r2_file = open(os.path.join(path, 'r2.txt'), 'w')
    r2_genomic_file = open(os.path.join(path, 'r2_genomic.txt'), 'w')
    for c in cell_types:
        model1 = model_builder(num_filters, filter_length)
        X_train, X_val, Y_train, Y_val = None, None, None, None
        if not use_both_regions:
            X_train, X_val, Y_train, Y_val = load_data_for_training(c)
        else:
            X_train, X_val, Y_train, Y_val = load_data_for_training_2(c)
        model1 = train_network(X_train, Y_train, X_val, Y_val, model1)
        predictions = model1.predict(X_val)
        r2_file.write(c + '\t' + str(r2_score(Y_val, predictions)) + '\n')
        print 'test r2:', r2_score(Y_val, predictions)
        model1.save_weights(os.path.join(path, '{0}.h5'.format(c)))
        model_json = model1.to_json()
        with open(os.path.join(path, '{0}.json'.format(c)), 'w') as f:
            f.write(model_json)
        try:
            preds = genomic_data.get_delta_psis(model1, wt_seqs, mut_seqs, wt_psi)
            print 'genomic r2:', r2_score(actual_dpsi, preds)
            r2_genomic_file.write(c + '\t' + str(r2_score(actual_dpsi, preds)) + '\n')
        except:
            print 'positional model: cannot use genomic'
        for layer_id in conv_layer_ids:
            avg_filters = draw_logo.get_avg_filters(model1, X_val,
                    model1.layers[layer_id])
            np.save(os.path.join(path, '{0}-filters-{1}'.format(c, layer_id)), avg_filters)
            if len(conv_layer_ids)==1:
                # because idk how to get influence working for functional models...
                influences = draw_logo.get_influence(model1, X_val, layer_id)
                np.save(os.path.join(path, '{0}-influence-{1}'.format(c, layer_id)), influences)
            else:
                pass
        del X_train
        del Y_train
        del X_val
        del Y_val
    r2_file.close()
    r2_genomic_file.close()

def get_filters_from_models(path, conv_layer_ids=[0]):
    cell_types = ['HEPG2', 'MCF7', 'CHO', 'HELA', 'LNCAP', 'HEK']
    for c in cell_types:
        X_train, X_val, Y_train, Y_val = load_data_for_training(c)
        # load from json
        model1 = None
        with open(os.path.join(path, '{0}.json'.format(c))) as f:
            model1 = model_from_json(f.read())
        model1.load_weights(os.path.join(path, c + '.h5'))
        for layer_id in conv_layer_ids:
            avg_filters = draw_logo.get_avg_filters(model1, X_val, model1.layers[layer_id])
            np.save(os.path.join(path, c+'-filters'), avg_filters)
            influences = draw_logo.get_influence(model1, X_val)
            np.save(os.path.join(path, '{0}-influence-{1}'.format(c, layer_id)), influences)

def compare_predictions(path):
    """
    Loads each of the models, compares predictions using all cell types.
    """
    # TODO

if __name__=='__main__':
    cell_types = ['HEPG2', 'MCF7', 'CHO', 'HELA', 'LNCAP', 'HEK']
    #train_all(build_global_max_model, '../data/2017-04-14-models-2')
    #train_all(build_global_mean_model, '../data/2017-04-15-models-2')
    train_all(build_global_2_max_model, '../data/2017-04-14-models-3', num_filters=[100,64], filter_length=[6,4])
    train_all(build_global_2_mean_model, '../data/2017-04-15-models-3', num_filters=[100,64], filter_length=[6,4])
    train_all(build_global_max_model, '../data/2017-04-27-models-2', num_filters=[500])
    #train_all(build_global_max_model, '../data/2017-04-27-models', num_filters=[500])
    train_all(build_global_max_avg_model, '../data/2017-04-28-models-2', num_filters=[100, 64], filter_length=[6,4], conv_layer_ids = [1,2])
    train_all(build_global_positional_model, '../data/2017-05-01-models-2', num_filters=[200, 64])
    train_all(build_recurrent_model, '../data/2017-05-02-models-2', num_filters=[200, 64])
    #train_all(build_recurrent_model, '../data/2017-05-03-models', num_filters=[200, 10])
    #get_filters_from_models('../data/2017-04-15-models')
