#!/bin/sh

jupyter nbconvert --to html */results/2017-*.ipynb
bash build_index.sh > index.html
mv index.html notebooks/
mv 2017-*.html notebooks/
cp -r */results/seq_logos notebooks/

# TODO: make an index file

rsync notebooks/*.html yjzhang@attu.cs.washington.edu:~/yjzhang/notebooks/
rsync -r notebooks/seq_logos yjzhang@attu.cs.washington.edu:~/yjzhang/notebooks/
