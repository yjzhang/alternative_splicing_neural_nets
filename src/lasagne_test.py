"""
Notes: testing lasagne
"""

import time

import pandas as pd
import numpy as np
import scipy.io as sio
from pylab import *

import joblib

import theano
import theano.tensor as T
import lasagne

from sklearn.cross_validation import train_test_split
from sklearn.metrics import r2_score

# TODO: implement functions for doing learnings 

def seq_to_array(seq):
    """
    Given a string, this returns a 2d np array: axis 1 = sequence,
    axis 2 = character (A, T, C, G)
    """
    new_array = np.zeros((len(seq), 4))
    for i, s in enumerate(seq):
        if s=='A' or s=='a':
            new_array[i][0] = 1
        elif s=='T' or s=='t':
            new_array[i][1] = 1
        elif s=='C' or s=='c':
            new_array[i][2] = 1
        elif s=='G' or s=='g':
            new_array[i][3] = 1
    return new_array

def build_mlp(input_var=None, n_hidden_units=100):
    """
    Single-layer NN that uses just one of the degenerate regions.
    """
    # for now, just use a single region
    # the shape: 25 positions, 4 possibilities per position 
    l_in = lasagne.layers.InputLayer(shape=(None, 25, 4),
            input_var=input_var)
    # dropout?
    l_in_drop = lasagne.layers.DropoutLayer(l_in, p=0.2)
    # hidden layer - has a small number of units for now
    l_hid_1 = lasagne.layers.DenseLayer(
            l_in_drop, num_units=n_hidden_units,
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())
    # hidden layer dropout
    l_hid_1_drop = lasagne.layers.DropoutLayer(l_hid_1, p=0.5)
    # fully connected output layer - probability of splicing for each frame
    # 304 possible positions
    l_out = lasagne.layers.DenseLayer(
            l_hid_1_drop, num_units=304,
            nonlinearity = lasagne.nonlinearities.softmax)
    return l_out

def build_mlp_2(input_var=None, n_hidden_units=100, n_hidden_layers=1,
        n_output=304):
    """
    This uses both of the degenerate regions, and can have multiple
    hidden layers and any number of outputs.
    """
    # for now, just use a single region
    # the shape: 50 positions, 4 possibilities per position 
    l_in = lasagne.layers.InputLayer(shape=(None, 50, 4),
            input_var=input_var)
    # dropout?
    l_in_drop = lasagne.layers.DropoutLayer(l_in, p=0.2)
    # first hidden layer - I tried using softmax but it seemed to work poorly
    # rectify works better...
    l_hid_1 = lasagne.layers.DenseLayer(
            l_in_drop, num_units=n_hidden_units,
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())
    # hidden layer dropout
    l_hid_1_drop = lasagne.layers.DropoutLayer(l_hid_1, p=0.5)
    # create additional hidden layers
    for i in range(n_hidden_layers - 1):
        l_hid_i = lasagne.layers.DenseLayer(l_hid_1_drop,
                num_units=n_hidden_units,
                nonlinearity=lasagne.nonlinearities.rectify,
                W=lasagne.init.GlorotUniform())
        l_hid_i_drop = lasagne.layers.DropoutLayer(l_hid_i, p=0.5)
        l_hid_1_drop = l_hid_i_drop
    # fully connected output layer - probability of splicing for each frame
    # 304 possible positions
    l_out = lasagne.layers.DenseLayer(
            l_hid_1_drop, num_units=n_output,
            nonlinearity = lasagne.nonlinearities.softmax)
    return l_out

def build_cnn(input_var=None, n_output=1, n_filters=64,
        filter_size=6, pool_size=2):
    """
    Builds a convolutional neural network...
    Architecture based on DeepBind from Alipanahi et al. 2015.
    This architecture is a not-so-deep conv net with four stages:
        - conv stage - motif detectors
        - rectification stage
        - pooling
        - fully connected net
    """
    l_in = lasagne.layers.InputLayer(shape=(None, 4, 25),
            input_var=input_var)
    # convolutional layer - equivalent to taking n-mers, where
    # n is equal to the filter size?
    # how many filters should we have? This is sort of equivalent to the
    # number of effective n-mers? number of convolutional kernels
    l_conv = lasagne.layers.Conv1DLayer(l_in,
            num_filters=n_filters,
            filter_size=filter_size,
            nonlinearity=lasagne.nonlinearities.rectify)
    l_drop = lasagne.layers.DropoutLayer(l_conv, p=0.2)
    # add a dropout layer?
    # rectification layer is included in the convolution
    # pooling - how many filters to pool?
    # pooling mode: average or max?
    l_pool = lasagne.layers.Pool1DLayer(l_drop,
            pool_size=pool_size, mode='max')
    # output
    l_out = lasagne.layers.DenseLayer(l_pool,
            num_units=n_output,
            nonlinearity=lasagne.nonlinearities.softmax)
    return l_out

def build_cnn_2(input_var=None, n_output=2, layers=['conv', 'pool'],
        n_filters=[64], filter_sizes=[6], pool_sizes=[2],
        return_partial=False, input_length=25):
    """
    A more advanced CNN... with options for a lot of layers
    """
    try:
        l_in = lasagne.layers.InputLayer(shape=(None, 4, input_length),
                input_var=input_var)
    except:
        l_in = input_var
    last_layer = l_in
    for layer in layers:
        if layer=='conv':
            l_conv = lasagne.layers.Conv1DLayer(last_layer,
                    num_filters=n_filters[0],
                    filter_size=filter_sizes[0],
                    nonlinearity=lasagne.nonlinearities.rectify)
            n_filters = n_filters[1:]
            filter_sizes = filter_sizes[1:]
            last_layer = l_conv
        elif layer=='pool':
            l_pool = lasagne.layers.Pool1DLayer(last_layer,
                    pool_size=pool_sizes[0], mode='max')
            pool_sizes = pool_sizes[1:]
            last_layer = l_pool
        elif layer=='dense':
            l_dense = lasagne.layers.DenseLayer(last_layer,
                    n_units=n_filters[0],
                    nonlinearity=lasagne.nonlinearities.rectify)
            n_filters = n_filters[1:]
            last_layer = l_dense
        elif layer=='drop':
            l_drop = lasagne.layers.DropoutLayer(last_layer, p=0.2)
            last_layer = l_drop
    if return_partial:
        return last_layer
    l_out = lasagne.layers.DenseLayer(last_layer,
            num_units=n_output,
            nonlinearity=lasagne.nonlinearities.softmax)
    return l_out

def build_double_cnn(input_var=None, n_output=2, layers=['conv','pool'],
        post_layers=[], n_filters=[64], filter_sizes=[6], pool_sizes=[2],
        input_length=25):
    """
    Creates a CNN with two input streams, which should correspond to the
    two degenerate regions.
    """
    input1 = build_cnn_2(input_var[:,:,0:input_length], n_output,
            layers, n_filters,
            filter_sizes, pool_sizes, return_partial=True,
            input_length=input_length)
    input2 = build_cnn_2(input_var[:,:,input_length:], n_output,
            layers, n_filters,
            filter_sizes, pool_sizes, return_partial=True,
            input_length=input_length)
    l_concat = lasagne.layers.ConcatLayer((input1, input2))
    l_out = lasagne.layers.DenseLayer(l_concat, num_units=n_output,
            nonlinearity=lasagne.nonlinearities.softmax)
    return l_out

def build_lstm(input_var=None, n_output=2, layers=['conv','pool'],
        post_layers=[], n_filters=[64], filter_sizes=[6], pool_sizes=[2],
        n_lstm_units=[32], input_length=25):
    """
    Builds a model where the first layer is an lstm
    """
    l_input1 = lasagne.layers.InputLayer(shape=(None,input_length,4),
            input_var=input_var[:,0:input_length,:])
    l_lstm_1 = lasagne.layers.GRULayer(l_input1, num_units=n_lstm_units[0])
    l_reshape_1 = lasagne.layers.DimshuffleLayer(l_lstm_1, (0,2,1))
    l_input2 = lasagne.layers.InputLayer(shape=(None,input_length,4),
            input_var=input_var[:,input_length: ,:])
    l_lstm_2 = lasagne.layers.GRULayer(l_input2, num_units=n_lstm_units[0])
    l_reshape_2 = lasagne.layers.DimshuffleLayer(l_lstm_2, (0,2,1))
    l_cnn_1 = build_cnn_2(l_reshape_1, n_output,
            layers, n_filters,
            filter_sizes, pool_sizes, return_partial=True,
            input_length=input_length)
    l_cnn_2 = build_cnn_2(l_reshape_2, n_output,
            layers, n_filters,
            filter_sizes, pool_sizes, return_partial=True,
            input_length=input_length)
    l_concat = lasagne.layers.ConcatLayer((l_cnn_1, l_cnn_2))
    l_out = lasagne.layers.DenseLayer(l_concat, num_units=n_output,
            nonlinearity=lasagne.nonlinearities.softmax)
    return l_out

# ############################# Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]

def construct_training_functions(build_mlp_fn, learning_rate=0.01, **kwargs):
    """
    Returns two theano functions, one for training and
    the other for validation. build_mlp_fn is a function that returns the
    output layer of a NN. **kwargs are the arguments to be passed to
    build_mlp_fn in addition to the first argument input_var.
    """
    # Setting up the neural network for training
    # see https://github.com/Lasagne/Lasagne/blob/master/examples/mnist.py

    input_var = T.tensor3('inputs')
    target_var = T.matrix('targets')
    network = build_mlp_fn(input_var, **kwargs)

    # Create a loss expression for training, i.e., a scalar objective we want
    # to minimize (for our multi-class problem, it is the cross-entropy loss):
    prediction = lasagne.layers.get_output(network)
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
    loss = loss.mean()

    # Create update expressions for training, i.e., how to modify the
    # parameters at each training step. Here, we'll use Stochastic Gradient
    # Descent (SGD) with Nesterov momentum, but Lasagne offers plenty more.
    params = lasagne.layers.get_all_params(network, trainable=True)
    # TODO: change learning rate
    updates = lasagne.updates.nesterov_momentum(
                    loss, params, learning_rate=learning_rate, momentum=0.9)

    # Create a loss expression for validation/testing. The crucial difference
    # here is that we do a deterministic forward pass through the network,
    # disabling dropout layers.
    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                            target_var)
    test_loss = test_loss.mean()
    # Compile a function performing a training step on a mini-batch (by giving
    # the updates dictionary) and returning the corresponding training loss:
    train_fn = theano.function([input_var, target_var], loss, updates=updates)

    # Compile a second function computing the validation loss:
    val_fn = theano.function([input_var, target_var], test_loss)
    # compile a third function that just gives the prediction
    test_fn = theano.function([input_var], test_prediction)
    return (train_fn, val_fn, test_fn, network)

def train(X_train, Y_train, X_test, Y_test, num_epochs, train_fn, test_fn,
        network=None):
    """
    Trains the neural net that is encompassed by the provided train_fn
    and test_fn.
    """
    x_vals = X_test.values
    x_vals = np.dstack(x_vals)
    x_vals = np.rollaxis(x_vals, 2)
    for epoch in range(num_epochs):
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(X_train, Y_train, 500, shuffle=True):
            inputs, targets = batch
            inputs = np.dstack(inputs.values)
            inputs = np.rollaxis(inputs, 2)
            train_err += train_fn(inputs, targets)
            train_batches += 1
        results = test_fn(x_vals)
        r2 = r2_score(Y_test, results)
        # Then we print the results for this epoch:
        print("Epoch {} of {} took {:.3f}s".format(
                    epoch + 1, num_epochs, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err))
        #print("  validation loss:\t\t{:.5f}".format(val_err))
        print("  R^2 score:\t\t{:.6f}".format(r2))
        print("  SD1 r2:\t\t{:.6f}".format(r2_score(Y_test[:, 0], results[:,0])))
        print("  SD2 r2:\t\t{:.6f}".format(r2_score(Y_test[:, 1], results[:,1])))
        print("  SD3 r2:\t\t{:.6f}".format(r2_score(Y_test[:, 2], results[:,2])))
        # TODO: save temporary params
        if network is not None:
            params = lasagne.layers.get_all_param_values(network)
            joblib.dump(params, "../data/CNN_train_temp", compress=True)

def load_data(flank=0):
    """
    Loads the data...
    """
    data = sio.loadmat('../data/Reads.mat')
    # A5SS_data: numpy array containing a row for each sequence and a column for
    # each base pair position, where each value contains the proportion of
    # splicing events that occur at that particulaar position.
    A5SS_data = data['A5SS']
    A5SS_reads = np.array(A5SS_data.sum(1)).flatten()
    A5SS_data = np.array(A5SS_data.todense())
    # Get minigenes with reads
    A5SS_nn = find(A5SS_data.sum(axis=1))
    A5SS_reads = A5SS_reads[A5SS_nn]
    A5SS_data = A5SS_data[A5SS_nn]
    # A5SS_data is now normalized so that each row sums to 1
    A5SS_data = A5SS_data/A5SS_data.sum(axis=1)[:,newaxis]
    # A5SS_seqs: all sequences
    A5SS_seqs = pd.read_csv('../data/A5SS_Seqs.csv',index_col=0).Seq[A5SS_nn]
    # A5SS_r1: first degenerate region
    A5SS_r1 = A5SS_seqs.str.slice(7-flank, 32+flank)
    # A5SS_r2: second degenerate region
    A5SS_r2 = A5SS_seqs.str.slice(50-flank, 75+flank)
    A5SS_r = A5SS_r1 + A5SS_r2
    A5SS_r_array = A5SS_r.map(seq_to_array)
    # set random_state to 0 to have repeatable trials
    X_train, X_test, Y_train, Y_test = \
                train_test_split(A5SS_r_array, \
                A5SS_data, test_size = 0.2, random_state=0)
    return (X_train, X_test, Y_train, Y_test)

def mlp_main():
    X_train, X_test, Y_train, Y_test = load_data()
    # taking only SD1 and SD2
    Y1_test = Y_test[:,0]
    Y1_train = Y_train[:,0]
    Y2_test = Y_test[:,44]
    Y2_train = Y_train[:,44]
    Y_test_small = np.dstack([Y1_test, Y2_test])
    Y_train_small = np.dstack([Y1_train, Y2_train])
    Y_test_small = Y_test_small.reshape(
            (Y_test_small.shape[1], Y_test_small.shape[2]))
    Y_train_small = Y_train_small.reshape(
            (Y_train_small.shape[1], Y_train_small.shape[2]))
    train_fn, val_fn, test_fn, network = construct_training_functions(
            build_mlp_2,
            n_hidden_units = 200,
            n_hidden_layers = 1,
            n_output = 2)
    train(X_train, Y_train_small, X_test, Y_test_small, 200, train_fn, test_fn)
    return network

def cnn_main(X_train, X_test, Y_train, Y_test):
    # taking only SD1 and SD2
    Y1_test = Y_test[:,0]
    Y1_train = Y_train[:,0]
    Y2_test = Y_test[:,44]
    Y2_train = Y_train[:,44]
    Y3_test = Y_test[:,79]
    Y3_train = Y_train[:,79]
    # since we're not predicting de novo splice sites, we sum over them.
    Yn_test = 1 - Y1_test - Y2_test - Y3_test #Y_test[:,303]
    Yn_train = 1 - Y1_train - Y2_train - Y3_train #Y_train[:,303]
    Y_test_small = np.dstack([Y1_test, Y2_test, Y3_test, Yn_test])
    Y_train_small = np.dstack([Y1_train, Y2_train, Y3_train, Yn_train])
    Y_test_small = Y_test_small.reshape(
            (Y_test_small.shape[1], Y_test_small.shape[2]))
    Y_train_small = Y_train_small.reshape(
            (Y_train_small.shape[1], Y_train_small.shape[2]))
    Y1_train = Y1_train.reshape((len(Y1_train), 1))
    Y1_test = Y1_test.reshape((len(Y1_test), 1))
    Y2_train = Y2_train.reshape((len(Y2_train), 1))
    Y2_test = Y2_test.reshape((len(Y2_test), 1))
    X1_train = X_train.map(lambda x: x[0:25])
    X1_train = X1_train.map(lambda x: np.rollaxis(x, 1))
    X1_test = X_test.map(lambda x: x[0:25])
    X1_test = X1_test.map(lambda x: np.rollaxis(x, 1))
    # comment the next two lines out for lstm, since with
    # lstms the default dimension orderings are strange
    #X_train = X_train.map(lambda x: np.rollaxis(x, 1))
    #X_test = X_test.map(lambda x: np.rollaxis(x, 1))
    print(X1_train[0].shape)
    layers_paper = ['conv','conv','pool','conv','pool']
    n_filters_paper = [30,40,50]
    filter_sizes_paper = [6,5,5]
    train_fn, val_fn, test_fn, network = construct_training_functions(
            build_lstm, learning_rate=0.01,
            layers=['conv','pool'],
            n_output = 4, n_filters=[64], filter_sizes=[6,5,5],
            pool_sizes=[2,2], input_length=35)
    train(X_train, Y_train_small, X_test, Y_test_small, 100,
            train_fn, test_fn, network)
    params = lasagne.layers.get_all_param_values(network)
    joblib.dump(params, "../data/gru_cnn_1", compress=True)
    x_vals = X_test.values
    x_vals = np.dstack(x_vals)
    x_vals = np.rollaxis(x_vals, 2)
    result = test_fn(x_vals)
    print("SD1 r2:\t\t{:.6f}".format(r2_score(Y_test_small[:,0], result[:,0])))
    print("SD2 r2:\t\t{:.6f}".format(r2_score(Y_test_small[:,1], result[:,1])))
    print("SD3 r2:\t\t{:.6f}".format(r2_score(Y_test_small[:,2], result[:,2])))
    return network, test_fn


if __name__ == '__main__':
    X_train, X_test, Y_train, Y_test = load_data()
    network, test_fn = cnn_main(X_train, X_test, Y_train, Y_test)
    params = lasagne.layers.get_all_param_values(network)
    joblib.dump(params, "../data/gru_cnn_1", compress=True)
    lasagne.layers.set_all_param_values(network, params)
